package io.github.toxicbyte.toxicdeath;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Base utility class
 * @author T0x1cByt3
 */
public class Utils 
{
    /**
     * Class meant to making a timer for
     * certain tasks
     * @author T0x1cByt3
     */
    public static class Timer
    {
        public static HashMap<String, HashMap<String, Long>> times = new HashMap<>();
        
        public static void addTime(String identifier, String name)
        {
            HashMap<String, Long> hash = new HashMap<>();
            if(times.containsKey(identifier))
            {
                hash = (HashMap<String, Long>) times.get(identifier).clone();
            }
            hash.put(name, System.nanoTime());
            times.put(identifier, hash);
        }
        public static void addTime(String identifier, String name, int more)
        {
            HashMap<String, Long> hash = new HashMap<>();
            if(times.containsKey(identifier))
            {
                hash = (HashMap<String, Long>) times.get(identifier).clone();
            }
            long toPut = TimeUnit.SECONDS.toNanos(more) + System.nanoTime();
            hash.put(name, toPut);
            times.put(identifier, hash);
        }
        public static long getTimePassed(String identifier, String name)
        {
            if(times.containsKey(identifier) && times.get(identifier).containsKey(name))
            {
                return TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() -times.get(identifier).get(name));
            }
            return -1;
        }
    }
}

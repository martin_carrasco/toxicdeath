package io.github.toxicbyte.toxicdeath;

import java.util.HashMap;
import java.util.UUID;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author T0x1cByt3
 */
class tListener implements Listener 
{
    private final int timeUntilPublic = 10;
    public static HashMap<UUID, DeathBed> deathb = new HashMap<>();
    private ToxicDeath plugin;
    public tListener(ToxicDeath plugin) 
    {
        this.plugin = plugin;
    }
    @EventHandler
    public void onDeath(PlayerDeathEvent e)
    {
        if(e.getEntity().hasPermission("toxicdeath.spawn"))
        {
            final Location loc = e.getEntity().getLocation().add(0, 1, 0);
            Inventory inv = plugin.getServer().createInventory(null, 45, e.getEntity().getDisplayName());
            for(ItemStack i : e.getDrops())
            {
                inv.addItem(i);
            }
            final DeathBed db = new DeathBed(e.getEntity(), loc, inv);
            db.make();
            e.setKeepInventory(true);
            e.getEntity().getInventory().clear();
            deathb.put(e.getEntity().getUniqueId(), db);
            e.getEntity().sendMessage("A death bed has been dropped where you died");
            Utils.Timer.addTime(e.getEntity().getUniqueId().toString(), "dead");
            plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable()
            {
                @Override
                public void run()
                {
                   db.destroy();
                }
            }, 20*60*1);
        }
    }
    @EventHandler
    public void onInteract(PlayerInteractEvent e)
    {
        if (e.getClickedBlock().getType() == Material.CARPET && e.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if(e.getPlayer().hasPermission("toxicdeath.open"))
            {
                DeathBed db = null;
                for(UUID uuid : deathb.keySet())
                {
                    Player p = plugin.getServer().getPlayer(uuid);
                    if(e.getClickedBlock().getLocation().equals(deathb.get(uuid).getLocation()))
                    {
                        db = deathb.get(uuid).clone();
                    }
                }
                if(db != null)
                {
                    if(deathb.get(e.getPlayer().getUniqueId()).getInventory().getTitle().equalsIgnoreCase(e.getPlayer().getDisplayName()))
                    {
                        e.getPlayer().openInventory(db.getInventory());
                    }
                    if(Utils.Timer.getTimePassed(e.getPlayer().getUniqueId().toString(), "dead") >= this.timeUntilPublic)
                    {
                        e.getPlayer().openInventory(db.getInventory());
                    }
                    else
                    {
                        if(e.getPlayer().hasPermission("toxicdeath.override")){e.getPlayer().openInventory(db.getInventory());}
                        e.getPlayer().sendMessage(ChatColor.RED 
                            + "You have to wait " 
                            + String.valueOf(10-Utils.Timer.getTimePassed(e.getPlayer().getUniqueId().toString(), "dead"))
                            + " more seconds before you can open this death bed!");
                    }
                }
            }
        }
    }
    
}

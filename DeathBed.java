package io.github.toxicbyte.toxicdeath;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * Primary deathbed class containing all methods
 * related to it
 * @author T0x1cByt3
 */
public class DeathBed
{
    private Location loc;
    private Player p;
    private Inventory inv;
    /**
     * Primary constructor
     * @param p Player who died
     * @param loc Location where the deathbed should spawn
     * @param inv Inventory to contain
     */
    public DeathBed(Player p, Location loc, Inventory inv)
    {
        this.p = p;
        this.loc = loc;
        this.inv = inv;
    }
    /**
     * Get DeathBed based off player
     * @param p Player who died
     */
    public DeathBed(Player p)
    {
        if(tListener.deathb.containsKey(p.getUniqueId()))
        {
            DeathBed db = tListener.deathb.get(p.getUniqueId());
            this.p = db.getPlayer();
            this.loc = db.getLocation();
            this.inv = db.getInventory();
        }
        try {
            throw new Exception("Deathbed not found for the player");
        } catch (Exception ex) {
            Logger.getLogger(DeathBed.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * 
     * @return Player who died 
     */
    public Player getPlayer(){return p;}
    /**
     * 
     * @return Deathbed inventory 
     */
    public Inventory getInventory(){return inv;}
        /**
     * 
     * @return Location of DeathBed 
     */
    public Location getLocation(){return loc;}
    /**
     * 
     * @return Time left before it's public 
     */
    public long getTimeLeft(){return Math.abs(10-Utils.Timer.getTimePassed(p.getUniqueId().toString(), "dead"));}
    /**
     * 
     * @param time Time left before it's public 
     */
    public void setTimeLeft(int time){Utils.Timer.addTime(p.getUniqueId().toString(), "dead", time);}
    public void make()
    {
        loc.getBlock().setType(Material.CARPET);
        tListener.deathb.put(p.getUniqueId(), this);
    }
    public void destroy()
    {
        loc.getBlock().setType(Material.AIR);
        if(tListener.deathb.containsKey(p.getUniqueId()))tListener.deathb.remove(p.getUniqueId());
    }
    @Override
    public DeathBed clone()
    {
        return new DeathBed(p, loc, inv);
    }
}
